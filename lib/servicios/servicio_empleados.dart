import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:prueba_cidenet/modelos/empleado.dart';
import 'package:prueba_cidenet/modelos/empleados.dart';

class ServicioEmpleados {
  final url = 'http://10.0.2.2:8080/empleados/';

  Future<void> listar(BuildContext context) async {
    final proEmpleados = Provider.of<Empleados>(context, listen: false);
    String opcion = 'listar';
    final respuesta = await http.get(Uri.parse(url + opcion));
    String cadena = respuesta.body.toString();
    final decodificado = jsonDecode(cadena);

    proEmpleados.eliminarTodo();
    for (var i = 0; i < decodificado.length; i++) {
      Empleado empleado = Empleado();
      empleado.id = decodificado[i]['id'];
      empleado.primerApellido = decodificado[i]['primerApellido'];
      empleado.segundoApellido = decodificado[i]['segundoApellido'];
      empleado.primerNombre = decodificado[i]['primerNombre'];
      empleado.otrosNombres = decodificado[i]['otrosNombres'];
      empleado.paisEmpleo = decodificado[i]['paisEmpleo'];
      empleado.tipoIdentificacion = decodificado[i]['tipoIdentificacion'];
      empleado.numeroIdentificacion = decodificado[i]['numeroIdentificacion'];
      empleado.correoElectronico = decodificado[i]['correoElectronico'];
      empleado.fechaIngreso = decodificado[i]['fechaIngreso'];
      empleado.area = decodificado[i]['area'];
      empleado.estado = decodificado[i]['estado'];
      empleado.fechaHoraRegistro = decodificado[i]['fechaHoraRegistro'];
      empleado.fechaHoraEdicion = decodificado[i]['fechaHoraEdicion'];
      proEmpleados.agregar(empleado);
    }
  }

  Future<void> guardar(BuildContext context) async {
    final proEmpleado = Provider.of<Empleado>(context, listen: false);
    String opcion = 'guardar';
    final direccion = Uri.parse(url + opcion);
    await http.post(
      direccion,
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: jsonEncode(<String, String>{
        "area": "${proEmpleado.area}",
        "correoElectronico": "${proEmpleado.correoElectronico}",
        "estado": "${proEmpleado.estado}",
        "fechaHoraEdicion": "",
        "fechaHoraRegistro": "${proEmpleado.fechaHoraRegistro}",
        "fechaIngreso": "${proEmpleado.fechaIngreso}",
        "numeroIdentificacion": "${proEmpleado.numeroIdentificacion}",
        "otrosNombres": "${proEmpleado.otrosNombres}",
        "paisEmpleo": "${proEmpleado.paisEmpleo}",
        "primerApellido": "${proEmpleado.primerApellido}",
        "primerNombre": "${proEmpleado.primerNombre}",
        "segundoApellido": "${proEmpleado.segundoApellido}",
        "tipoIdentificacion": "${proEmpleado.tipoIdentificacion}"
      }),
    );
  }

  Future<void> editar(BuildContext context) async {
    final proEmpleado = Provider.of<Empleado>(context, listen: false);
    String opcion = 'actualizar';
    final direccion = Uri.parse(url + opcion);
    await http.put(
      direccion,
      headers: <String, String>{
        "Content-Type": "application/json; charset=UTF-8",
      },
      body: jsonEncode(<String, dynamic>{
        "id": proEmpleado.id,
        "area": "${proEmpleado.area}",
        "correoElectronico": "${proEmpleado.correoElectronico}",
        "estado": "${proEmpleado.estado}",
        "fechaHoraEdicion": "${proEmpleado.fechaHoraEdicion}",
        "fechaHoraRegistro": "${proEmpleado.fechaHoraRegistro}",
        "fechaIngreso": "${proEmpleado.fechaIngreso}",
        "numeroIdentificacion": "${proEmpleado.numeroIdentificacion}",
        "otrosNombres": "${proEmpleado.otrosNombres}",
        "paisEmpleo": "${proEmpleado.paisEmpleo}",
        "primerApellido": "${proEmpleado.primerApellido}",
        "primerNombre": "${proEmpleado.primerNombre}",
        "segundoApellido": "${proEmpleado.segundoApellido}",
        "tipoIdentificacion": "${proEmpleado.tipoIdentificacion}"
      }),
    );
  }

  Future<void> eliminar(int id) async {
    String opcion = 'eliminar/${id.toString()}';
    await http.delete(Uri.parse(url + opcion));
  }

  Future<void> filtrar(
      BuildContext context, String tipo, String palabra) async {
    final proEmpleados = Provider.of<Empleados>(context, listen: false);
    String opcion = '';
    if (tipo == 'Primer Nombre') {
      opcion = 'primernombre/$palabra';
    } else if (tipo == 'Otros Nombres') {
      opcion = 'otrosnombres/$palabra';
    } else if (tipo == 'Segundo Apellido') {
      opcion = 'segundoapellido/$palabra';
    } else if (tipo == 'Primer Apellido') {
      opcion = 'primerapellido/$palabra';
    } else if (tipo == 'Tipo de Identificación') {
      opcion = 'tipoidentificacion/$palabra';
    } else if (tipo == 'Número de Identificación') {
      opcion = 'numeroidentificacion/$palabra';
    } else if (tipo == 'País del empleo') {
      opcion = 'paisempleo/$palabra';
    } else if (tipo == 'Correo electrónico') {
      opcion = 'correoelectronico/$palabra';
    }
    final respuesta = await http.get(Uri.parse(url + opcion));
    String cadena = respuesta.body.toString();
    final decodificado = jsonDecode(cadena);

    proEmpleados.eliminarTodo();
    for (var i = 0; i < decodificado.length; i++) {
      Empleado empleado = Empleado();
      empleado.id = decodificado[i]['id'];
      empleado.primerApellido = decodificado[i]['primerApellido'];
      empleado.segundoApellido = decodificado[i]['segundoApellido'];
      empleado.primerNombre = decodificado[i]['primerNombre'];
      empleado.otrosNombres = decodificado[i]['otrosNombres'];
      empleado.paisEmpleo = decodificado[i]['paisEmpleo'];
      empleado.tipoIdentificacion = decodificado[i]['tipoIdentificacion'];
      empleado.numeroIdentificacion = decodificado[i]['numeroIdentificacion'];
      empleado.correoElectronico = decodificado[i]['correoElectronico'];
      empleado.fechaIngreso = decodificado[i]['fechaIngreso'];
      empleado.area = decodificado[i]['area'];
      empleado.estado = decodificado[i]['estado'];
      empleado.fechaHoraRegistro = decodificado[i]['fechaHoraRegistro'];
      empleado.fechaHoraEdicion = decodificado[i]['fechaHoraEdicion'];
      proEmpleados.agregar(empleado);
    }
  }
}
