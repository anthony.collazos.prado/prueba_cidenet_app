import 'package:prueba_cidenet/modelos/empleados.dart';

String validarNombre(String cadena) {
  String mensaje = '';
  RegExp regExp = RegExp('^[A-Z ]+\$');
  if (!regExp.hasMatch(cadena)) {
    mensaje =
        'Solo se permiten caracteres de la A a la Z en mayúscula, sin acentos ni Ñ';
  }
  if (cadena.length >= 20) {
    mensaje = 'Longitud máxima de 20 letras';
  }
  return mensaje;
}

String validarOtrosNombres(String cadena) {
  String mensaje = '';
  RegExp regExp = RegExp('^[A-Z ]+\$');
  if (!regExp.hasMatch(cadena)) {
    mensaje =
        'Solo se permiten caracteres de la A a la Z en mayúscula, sin acentos ni Ñ';
  }
  if (cadena.length >= 50) {
    mensaje = 'Longitud máxima de 50 letras';
  }
  return mensaje;
}

String validarNumeroIdentificacion(String cadena) {
  String mensaje = '';
  RegExp regExp = RegExp('^[A-Za-z0-9 ]+\$');
  if (!regExp.hasMatch(cadena)) {
    mensaje =
        'Solo se permiten los siguientes conjuntos de caracteres: a-z, A-Z, 0-9';
  }
  if (cadena.length >= 20) {
    mensaje = 'Longitud máxima de 20 letras';
  }
  return mensaje;
}

String generarCorreo(
    Empleados empleados, String primerNombre, String primerApellido) {
  bool existeCorreo = false;
  String nombre = primerNombre.toLowerCase().replaceAll(' ', '');
  String apellido = primerApellido.toLowerCase().replaceAll(' ', '');
  String correo = '$nombre.$apellido@cidenet.com.co';
  String correoFinal = '';
  existeCorreo = validarCorreo(empleados, correo);
  if (existeCorreo) {
    List<String> cadena1 = correo.split('@');
    List<String> cadena2 = cadena1[0].split('.');
    int numero = 1;
    correoFinal =
        '${cadena2[0]}.${cadena2[1]}.${numero.toString()}@cidenet.com.co';
    while (validarCorreo(empleados, correoFinal)) {
      numero++;
      correoFinal =
          '${cadena2[0]}.${cadena2[1]}.${numero.toString()}@cidenet.com.co';
    }
  } else {
    correoFinal = correo;
  }
  return correoFinal;
}

bool validarCorreo(Empleados empleados, String correo) {
  bool existeCorreo = false;
  for (var i = 0; i < empleados.items.length; i++) {
    if (empleados.items[i].correoElectronico == correo) {
      existeCorreo = true;
      break;
    } else {
      existeCorreo = false;
    }
  }
  return existeCorreo;
}

String formatoFechaI(DateTime? fecha) {
  String respuesta = fecha.toString();
  List<String> lisA = respuesta.split(' ');
  List<String> lisB = lisA[0].split('-');
  return respuesta = '${lisB[2]}/${lisB[1]}/${lisB[0]}';
}

String formatoFechaE(String? fecha) {
  String respuesta = fecha.toString();
  List<String> lisA = respuesta.split(' ');
  List<String> lisB = lisA[0].split('-');
  return respuesta = '${lisB[2]}/${lisB[1]}/${lisB[0]}';
}

String formatoFechaBD(String fecha) {
  List<String> lisA = fecha.split('/');
  String respuesta = '${lisA[2]}-${lisA[1]}-${lisA[0]}';
  return respuesta;
}

String formatoFechaG(String fecha) {
  String respuesta;
  respuesta = fecha.replaceAll(' ', 'T').substring(0, fecha.length - 3);
  respuesta = '$respuesta+00:00';
  return respuesta;
}
