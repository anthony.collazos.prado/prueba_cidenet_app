import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_cidenet/elementos/estilo_general.dart';
import 'package:prueba_cidenet/modelos/empleado.dart';
import 'package:prueba_cidenet/modelos/empleados.dart';
import 'package:prueba_cidenet/modelos/filtro.dart';
import 'package:prueba_cidenet/paginas/pagina_agregar.dart';
import 'package:prueba_cidenet/paginas/pagina_detalle.dart';
import 'package:prueba_cidenet/paginas/pagina_editar.dart';
import 'package:prueba_cidenet/paginas/pagina_empleados.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => Empleado()),
        ChangeNotifierProvider(create: (context) => Empleados()),
        ChangeNotifierProvider(create: (context) => Filtro()),
      ],
      child: const MiApp(),
    ),
  );
}

class MiApp extends StatelessWidget {
  const MiApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Prueba Wigilabs',
      theme: EstiloGeneral.tema,
      initialRoute: 'empleados',
      routes: {
        'empleados': (BuildContext context) => const PaginaEmpleados(),
        'detalle': (BuildContext context) => const PaginaDetalle(),
        'agregar': (BuildContext context) => const PaginaAgregar(),
        'editar': (BuildContext context) => const PaginaEditar(),
      },
    );
  }
}
