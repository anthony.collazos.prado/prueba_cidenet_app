import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_cidenet/elementos/color_general.dart';
import 'package:prueba_cidenet/elementos/elemento_general.dart';
import 'package:prueba_cidenet/elementos/estilo_general.dart';
import 'package:prueba_cidenet/modelos/empleados.dart';
import 'package:prueba_cidenet/utiles/funciones.dart';

class PaginaDetalle extends StatelessWidget {
  const PaginaDetalle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final indice = ModalRoute.of(context)!.settings.arguments;
    final pantalla = MediaQuery.of(context).size;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarGeneral(context),
      body: Container(
        constraints: const BoxConstraints.expand(),
        color: ColorGeneral.gris1,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: obtenerMarco(pantalla),
              ),
              child: Column(
                children: _contenido(context, pantalla, indice),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _contenido(BuildContext context, Size pantalla, dynamic indice) {
    final proEmpleados = Provider.of<Empleados>(context, listen: false);
    int i = int.parse(indice.toString());
    final data = proEmpleados.items[i];

    List<Widget> listado = [
      EstiloGeneral.espaciadorX3,
      Stack(
        children: [
          Container(
            width: obtenerAncho(pantalla),
            margin: const EdgeInsets.only(top: 100.0),
            padding: EstiloGeneral.primerPadding,
            decoration: EstiloGeneral.cajaBlanca,
            child: Column(
              children: [
                const SizedBox(height: 110.0),
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: '${data.primerNombre} ',
                        style: EstiloGeneral.h1NegroB,
                      ),
                      TextSpan(
                        text: '${data.otrosNombres} ',
                        style: EstiloGeneral.h1NegroB,
                      ),
                      TextSpan(
                        text: '${data.primerApellido} ',
                        style: EstiloGeneral.h1NegroB,
                      ),
                      TextSpan(
                        text: '${data.segundoApellido}\n',
                        style: EstiloGeneral.h1NegroB,
                      ),
                      TextSpan(
                        text: '${data.correoElectronico}\n',
                        style: EstiloGeneral.h4GrisN,
                      ),
                      TextSpan(
                        text: '${data.area}',
                        style: EstiloGeneral.h3CyanB,
                      ),
                    ],
                  ),
                ),
                EstiloGeneral.espaciadorX1,
                EstiloGeneral.divisor1,
                EstiloGeneral.espaciadorX1,
                RichText(
                  textAlign: TextAlign.center,
                  text: TextSpan(
                    children: <TextSpan>[
                      const TextSpan(
                        text: 'Mas información:\n\n',
                        style: EstiloGeneral.h3NegroB,
                      ),
                      const TextSpan(
                        text: 'País de empleo: ',
                        style: EstiloGeneral.h4GrisN,
                      ),
                      TextSpan(
                        text: '${data.paisEmpleo}\n',
                        style: EstiloGeneral.h4CyanB,
                      ),
                      const TextSpan(
                        text: 'Tipo de Identificación: ',
                        style: EstiloGeneral.h4GrisN,
                      ),
                      TextSpan(
                        text: '${data.tipoIdentificacion}\n',
                        style: EstiloGeneral.h4CyanB,
                      ),
                      const TextSpan(
                        text: 'Número de Identificación: ',
                        style: EstiloGeneral.h4GrisN,
                      ),
                      TextSpan(
                        text: '${data.numeroIdentificacion}\n',
                        style: EstiloGeneral.h4CyanB,
                      ),
                      const TextSpan(
                        text: 'Fecha de ingreso: ',
                        style: EstiloGeneral.h4GrisN,
                      ),
                      TextSpan(
                        text: '${formatoFechaE(data.fechaIngreso)}\n',
                        style: EstiloGeneral.h4CyanB,
                      ),
                      const TextSpan(
                        text: 'Estado: ',
                        style: EstiloGeneral.h4GrisN,
                      ),
                      TextSpan(
                        text: '${data.estado}',
                        style: EstiloGeneral.h4CyanB,
                      ),
                    ],
                  ),
                ),
                EstiloGeneral.espaciadorX2,
                Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: EstiloGeneral.circuloCyan,
                  child: Center(
                    child: IconButton(
                      icon: const Icon(
                        Icons.keyboard_arrow_left,
                        color: Colors.white,
                      ),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                  ),
                ),
                EstiloGeneral.espaciadorX1,
              ],
            ),
          ),
          Center(
            child: CircleAvatar(
              radius: 100.0,
              backgroundImage: AssetImage(
                'assets/images/empleado_${i + 1}_cidenet.jpg',
              ),
            ),
          ),
        ],
      ),
    ];

    return listado;
  }
}
