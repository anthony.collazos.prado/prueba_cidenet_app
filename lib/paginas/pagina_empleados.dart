import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_cidenet/elementos/color_general.dart';
import 'package:prueba_cidenet/elementos/elemento_general.dart';
import 'package:prueba_cidenet/elementos/estilo_general.dart';
import 'package:prueba_cidenet/modelos/empleados.dart';
import 'package:prueba_cidenet/modelos/filtro.dart';
import 'package:prueba_cidenet/servicios/servicio_empleados.dart';

class PaginaEmpleados extends StatefulWidget {
  const PaginaEmpleados({Key? key}) : super(key: key);

  @override
  PaginaEmpleadosState createState() => PaginaEmpleadosState();
}

class PaginaEmpleadosState extends State<PaginaEmpleados> {
  int paginaCargada = 0;
  int paginaActual = 1;
  int paginaActualBotones = 1;

  @override
  Widget build(BuildContext context) {
    Widget objeto;
    final servicioEmpleados = ServicioEmpleados();
    final pantalla = MediaQuery.of(context).size;

    if (paginaCargada == 0) {
      objeto = FutureBuilder(
        future: servicioEmpleados.listar(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return pantallaCargando();
          } else {
            return Scaffold(
              extendBodyBehindAppBar: true,
              appBar: appBarGeneral(context),
              body: Container(
                constraints: const BoxConstraints.expand(),
                color: ColorGeneral.gris1,
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: obtenerMarco(pantalla),
                      ),
                      child: Column(
                        children: _contenido(context, pantalla),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        },
      );
    } else if (paginaCargada == 2) {
      final proFiltro = Provider.of<Filtro>(context, listen: false);

      objeto = FutureBuilder(
        future: servicioEmpleados.filtrar(
            context, proFiltro.opcion.toString(), proFiltro.palabra.toString()),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return pantallaCargando();
          } else {
            return Scaffold(
              extendBodyBehindAppBar: true,
              appBar: appBarGeneral(context),
              body: Container(
                constraints: const BoxConstraints.expand(),
                color: ColorGeneral.gris1,
                child: SafeArea(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: obtenerMarco(pantalla),
                      ),
                      child: Column(
                        children: _contenido(context, pantalla),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
        },
      );
    } else {
      objeto = Scaffold(
        extendBodyBehindAppBar: true,
        appBar: appBarGeneral(context),
        body: Container(
          constraints: const BoxConstraints.expand(),
          color: ColorGeneral.gris1,
          child: SafeArea(
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: obtenerMarco(pantalla),
                ),
                child: Column(
                  children: _contenido(context, pantalla),
                ),
              ),
            ),
          ),
        ),
      );
    }
    return objeto;
  }

  List<Widget> _contenido(BuildContext context, Size pantalla) {
    final proEmpleados = Provider.of<Empleados>(context, listen: false);
    int porPagina = 10;
    int longitud = proEmpleados.items.length;
    int cantidadPaginas = _obtenerPaginas(
      longitud,
      porPagina,
    );
    List<int> limites = _obtenerLimites(
      longitud,
      paginaActual,
      cantidadPaginas,
      porPagina,
    );

    List<Widget> listado = [
      EstiloGeneral.espaciadorX3,
      const Text('Lista de Empleados', style: EstiloGeneral.h1AzulB),
      EstiloGeneral.espaciadorX2,
      Row(
        children: <Widget>[
          TextButton.icon(
            icon: const Icon(Icons.add, color: ColorGeneral.cyan),
            label: const Text('Agregar', style: EstiloGeneral.h3CyanB),
            onPressed: () => Navigator.pushNamed(context, 'agregar'),
          ),
          const Spacer(),
          _botonAtras(),
          _botonAdelante(cantidadPaginas),
          _botonFiltrar(context),
        ],
      ),
      EstiloGeneral.espaciadorX1,
    ];

    for (var i = limites[0]; i < limites[1]; i++) {
      final data = proEmpleados.items[i];

      listado.add(
        ElevatedButton(
          style: EstiloGeneral.botonBlanco,
          child: SizedBox(
            width: obtenerAncho(pantalla),
            child: Padding(
              padding: EstiloGeneral.primerPadding,
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    radius: 40.0,
                    backgroundImage: AssetImage(
                      'assets/images/empleado_${i + 1}_cidenet.jpg',
                    ),
                  ),
                  EstiloGeneral.espaciadorY1,
                  Expanded(
                    child: RichText(
                      text: TextSpan(
                        children: <TextSpan>[
                          TextSpan(
                            text: '${data.primerNombre} ',
                            style: EstiloGeneral.h3NegroB,
                          ),
                          TextSpan(
                            text: '${data.primerApellido}\n',
                            style: EstiloGeneral.h3NegroB,
                          ),
                          TextSpan(
                            text: '${data.correoElectronico}\n',
                            style: EstiloGeneral.tGrisN,
                          ),
                          TextSpan(
                            text: '${data.area}',
                            style: EstiloGeneral.h4CyanB,
                          ),
                        ],
                      ),
                    ),
                  ),
                  EstiloGeneral.espaciadorY1,
                  Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: EstiloGeneral.circuloCyan,
                    child: IconButton(
                      icon: const Icon(
                        Icons.edit_outlined,
                        color: Colors.white,
                      ),
                      onPressed: () => Navigator.pushNamed(
                        context,
                        'editar',
                        arguments: i,
                      ),
                    ),
                  ),
                  EstiloGeneral.espaciadorY1,
                  Container(
                    width: 40.0,
                    height: 40.0,
                    decoration: EstiloGeneral.circuloRojo,
                    child: IconButton(
                      icon: const Icon(
                        Icons.delete_outline,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        mostrarMensaje(
                          context,
                          '¡Atención!',
                          '¿Está seguro de que desea eliminar el empleado?',
                          int.parse(data.id.toString()),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          onPressed: () => Navigator.pushNamed(
            context,
            'detalle',
            arguments: i,
          ),
        ),
      );
      listado.add(EstiloGeneral.espaciadorX1);
    }

    listado.add(EstiloGeneral.espaciadorX1);
    listado.add(Wrap(children: _botonesPaginas(cantidadPaginas)));
    listado.add(EstiloGeneral.espaciadorX2);

    return listado;
  }

  int _obtenerPaginas(int longitud, int porPagina) {
    double resultado = longitud / porPagina;
    int cantidadPaginas = resultado.ceil();
    return cantidadPaginas;
  }

  List<int> _obtenerLimites(
      int longitud, int numeroPagina, int cantidadPaginas, int porPagina) {
    int maximo;
    int modulo = longitud % porPagina;
    int limiteInferior = numeroPagina * porPagina - porPagina;
    int limiteSuperior = numeroPagina * porPagina;
    (modulo == 0)
        ? maximo = cantidadPaginas * porPagina
        : maximo = cantidadPaginas * porPagina - porPagina;
    if (limiteSuperior > maximo && modulo > 0) {
      limiteSuperior = limiteInferior + modulo;
    }
    List<int> resultado = [limiteInferior, limiteSuperior];
    return resultado;
  }

  Widget _botonAtras() {
    Widget boton;
    if (paginaActual > 1) {
      boton = IconButton(
        icon: const Icon(
          Icons.keyboard_arrow_left,
          color: ColorGeneral.cyan,
        ),
        padding: EdgeInsets.zero,
        onPressed: () => setState(() => paginaActual--),
      );
    } else {
      boton = const SizedBox(width: 0.0);
    }
    return boton;
  }

  Widget _botonAdelante(int cantidadPaginas) {
    Widget boton;
    if (paginaActual < cantidadPaginas) {
      boton = IconButton(
        icon: const Icon(
          Icons.keyboard_arrow_right,
          color: ColorGeneral.cyan,
        ),
        padding: EdgeInsets.zero,
        onPressed: () => setState(() => paginaActual++),
      );
    } else {
      boton = const SizedBox(width: 0.0);
    }
    return boton;
  }

  Widget _botonFiltrar(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.search, color: ColorGeneral.cyan),
      padding: EdgeInsets.zero,
      onPressed: () {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            final _valorBusqueda = TextEditingController();
            String? _valorOpcion;
            return AlertDialog(
              contentPadding: EstiloGeneral.primerPadding,
              content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
                  return Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: const <Widget>[
                          Icon(
                            Icons.message_outlined,
                            color: ColorGeneral.cyan,
                          ),
                          EstiloGeneral.espaciadorY1,
                          Text('Filtrar', style: EstiloGeneral.h2CyanB),
                        ],
                      ),
                      EstiloGeneral.espaciadorX1,
                      EstiloGeneral.divisor1,
                      EstiloGeneral.espaciadorX1,
                      DropdownButton<String>(
                        isExpanded: true,
                        hint: const Text(
                          'Opción de búsqueda',
                          style: EstiloGeneral.h3GrisN,
                        ),
                        value: _valorOpcion,
                        icon: const Icon(
                          Icons.keyboard_arrow_down,
                          color: ColorGeneral.gris2,
                        ),
                        underline: const SizedBox(),
                        onChanged: (String? valorNuevo) {
                          setState(() => _valorOpcion = valorNuevo);
                        },
                        items: <String>[
                          'Primer Nombre',
                          'Otros Nombres',
                          'Primer Apellido',
                          'Segundo Apellido',
                          'Tipo de Identificación',
                          'Número de Identificación',
                          'País del empleo',
                          'Correo electrónico',
                        ].map<DropdownMenuItem<String>>((String valor) {
                          return DropdownMenuItem<String>(
                            value: valor,
                            child: Text(valor, style: EstiloGeneral.h3NegroN),
                          );
                        }).toList(),
                      ),
                      EstiloGeneral.espaciadorX1,
                      TextFormField(
                        controller: _valorBusqueda,
                        decoration: const InputDecoration(
                          labelText: 'Palabra a buscar',
                          border: InputBorder.none,
                          labelStyle: TextStyle(color: ColorGeneral.gris2),
                        ),
                        onChanged: (String valor) {
                          setState(() {
                            _valorBusqueda.text = valor;
                            _valorBusqueda.selection =
                                TextSelection.fromPosition(
                              TextPosition(offset: _valorBusqueda.text.length),
                            );
                          });
                        },
                      ),
                    ],
                  );
                },
              ),
              actions: [
                TextButton(
                  onPressed: () async {
                    final proFiltro =
                        Provider.of<Filtro>(context, listen: false);
                    proFiltro.opcion = _valorOpcion.toString();
                    proFiltro.palabra = _valorBusqueda.text;
                    proFiltro.actualizar();
                    paginaCargada = 2;
                    Navigator.pop(context);
                  },
                  child: const Text('FILTRAR', style: EstiloGeneral.h3AzulB),
                ),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamedAndRemoveUntil(
                      context,
                      'empleados',
                      (Route<dynamic> route) => false,
                    );
                  },
                  child: const Text('CERRAR', style: EstiloGeneral.h3CyanB),
                ),
              ],
            );
          },
        );
      },
    );
  }

  List<Widget> _botonesPaginas(int cantidadPaginas) {
    List<Widget> botones = [];
    int porPagina = 5;
    int paginasBotones = _obtenerPaginas(cantidadPaginas, porPagina);
    List<int> limites = _obtenerLimites(
      cantidadPaginas,
      paginaActualBotones,
      paginasBotones,
      porPagina,
    );

    if (paginaActualBotones > 1) {
      botones.add(
        IconButton(
          icon: const Icon(
            Icons.keyboard_arrow_left,
            color: ColorGeneral.cyan,
          ),
          padding: EdgeInsets.zero,
          onPressed: () => setState(() => paginaActualBotones--),
        ),
      );
    }
    for (var i = limites[0] + 1; i <= limites[1]; i++) {
      botones.add(
        IconButton(
          icon: (paginaActual == i)
              ? Container(
                  decoration: EstiloGeneral.circuloCyan,
                  child: Center(
                    child: Text('$i', style: EstiloGeneral.tBlancoBoton),
                  ),
                )
              : Text('$i', style: EstiloGeneral.tGrisBoton),
          onPressed: () => setState(() => paginaActual = i),
        ),
      );
    }
    if (paginaActualBotones < paginasBotones) {
      botones.add(
        IconButton(
          icon: const Icon(
            Icons.keyboard_arrow_right,
            color: ColorGeneral.cyan,
          ),
          padding: EdgeInsets.zero,
          onPressed: () => setState(() => paginaActualBotones++),
        ),
      );
    }

    return botones;
  }
}
