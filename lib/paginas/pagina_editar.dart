import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:prueba_cidenet/elementos/color_general.dart';
import 'package:prueba_cidenet/elementos/elemento_general.dart';
import 'package:prueba_cidenet/elementos/estilo_general.dart';
import 'package:prueba_cidenet/modelos/empleado.dart';
import 'package:prueba_cidenet/modelos/empleados.dart';
import 'package:prueba_cidenet/servicios/servicio_empleados.dart';
import 'package:prueba_cidenet/utiles/funciones.dart';

class PaginaEditar extends StatefulWidget {
  const PaginaEditar({Key? key}) : super(key: key);

  @override
  PaginaEditarState createState() => PaginaEditarState();
}

class PaginaEditarState extends State<PaginaEditar> {
  final _valorPrimerApellido = TextEditingController();
  final _valorSegundoApellido = TextEditingController();
  final _valorPrimerNombre = TextEditingController();
  final _valorOtrosNombres = TextEditingController();
  final _valorNumeroIdentificacion = TextEditingController();
  final _valorFechaIngreso = TextEditingController();
  bool paginaCargada = false;
  String? _valorPaisEmpleo;
  String? _valorTipoIdentificacion;
  String? _valorArea;
  DateTime fecha = DateTime.now();

  @override
  Widget build(BuildContext context) {
    final pantalla = MediaQuery.of(context).size;
    final indice = ModalRoute.of(context)!.settings.arguments;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: appBarGeneral(context),
      body: Container(
        constraints: const BoxConstraints.expand(),
        color: ColorGeneral.gris1,
        child: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(
                horizontal: obtenerMarco(pantalla),
              ),
              child: Column(
                children: _contenido(context, pantalla, indice),
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _contenido(BuildContext context, Size pantalla, dynamic indice) {
    final proEmpleado = Provider.of<Empleado>(context, listen: false);
    final proEmpleados = Provider.of<Empleados>(context, listen: false);
    String? valFechaIni = 'dd/mm/aaaa';

    if (!paginaCargada) {
      _valorPrimerApellido.text =
          proEmpleados.items[indice].primerApellido.toString();
      _valorSegundoApellido.text =
          proEmpleados.items[indice].segundoApellido.toString();
      _valorPrimerNombre.text =
          proEmpleados.items[indice].primerNombre.toString();
      _valorOtrosNombres.text =
          proEmpleados.items[indice].otrosNombres.toString();
      _valorPaisEmpleo = proEmpleados.items[indice].paisEmpleo.toString();
      _valorTipoIdentificacion =
          proEmpleados.items[indice].tipoIdentificacion.toString();
      _valorNumeroIdentificacion.text =
          proEmpleados.items[indice].numeroIdentificacion.toString();
      _valorFechaIngreso.text =
          formatoFechaE(proEmpleados.items[indice].fechaIngreso);
      _valorArea = proEmpleados.items[indice].area.toString();
    }

    List<Widget> listado = [
      EstiloGeneral.espaciadorX3,
      Container(
        width: 100.0,
        height: 100.0,
        decoration: EstiloGeneral.circuloCyan,
        child: const Icon(
          Icons.edit_outlined,
          color: Colors.white,
          size: 50.0,
        ),
      ),
      EstiloGeneral.espaciadorX2,
      const Text('Editar Empleado', style: EstiloGeneral.h1CyanB),
      EstiloGeneral.espaciadorX2,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: TextFormField(
          controller: _valorPrimerApellido,
          decoration: const InputDecoration(
            labelText: 'Primer Apellido',
            border: InputBorder.none,
            labelStyle: TextStyle(color: ColorGeneral.gris2),
          ),
          onChanged: (String valor) {
            String respuesta = validarNombre(valor);
            if (respuesta.isNotEmpty) {
              mostrarValidacion(context, '¡Atención!', respuesta);
            } else {
              setState(() {
                _valorPrimerApellido.text = valor;
                _valorPrimerApellido.selection = TextSelection.fromPosition(
                  TextPosition(offset: _valorPrimerApellido.text.length),
                );
                paginaCargada = true;
              });
            }
          },
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: TextFormField(
          controller: _valorSegundoApellido,
          decoration: const InputDecoration(
            labelText: 'Segundo Apellido',
            border: InputBorder.none,
            labelStyle: TextStyle(color: ColorGeneral.gris2),
          ),
          onChanged: (String valor) {
            String respuesta = validarNombre(valor);
            if (respuesta.isNotEmpty) {
              mostrarValidacion(context, '¡Atención!', respuesta);
            } else {
              setState(() {
                _valorSegundoApellido.text = valor;
                _valorSegundoApellido.selection = TextSelection.fromPosition(
                  TextPosition(offset: _valorSegundoApellido.text.length),
                );
                paginaCargada = true;
              });
            }
          },
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: TextFormField(
          controller: _valorPrimerNombre,
          decoration: const InputDecoration(
            labelText: 'Primer Nombre',
            border: InputBorder.none,
            labelStyle: TextStyle(color: ColorGeneral.gris2),
          ),
          onChanged: (String valor) {
            String respuesta = validarNombre(valor);
            if (respuesta.isNotEmpty) {
              mostrarValidacion(context, '¡Atención!', respuesta);
            } else {
              setState(() {
                _valorPrimerNombre.text = valor;
                _valorPrimerNombre.selection = TextSelection.fromPosition(
                  TextPosition(offset: _valorPrimerNombre.text.length),
                );
                paginaCargada = true;
              });
            }
          },
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: TextFormField(
          controller: _valorOtrosNombres,
          decoration: const InputDecoration(
            labelText: 'Otros Nombres',
            border: InputBorder.none,
            labelStyle: TextStyle(color: ColorGeneral.gris2),
          ),
          onChanged: (String valor) {
            String respuesta = validarOtrosNombres(valor);
            if (respuesta.isNotEmpty) {
              mostrarValidacion(context, '¡Atención!', respuesta);
            } else {
              setState(() {
                _valorOtrosNombres.text = valor;
                _valorOtrosNombres.selection = TextSelection.fromPosition(
                  TextPosition(offset: _valorOtrosNombres.text.length),
                );
                paginaCargada = true;
              });
            }
          },
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: DropdownButton<String>(
          isExpanded: true,
          hint: const Text(
            'País del empleo',
            style: EstiloGeneral.h3GrisN,
          ),
          value: _valorPaisEmpleo,
          icon: const Icon(
            Icons.keyboard_arrow_down,
            color: ColorGeneral.gris2,
          ),
          underline: const SizedBox(),
          onChanged: (String? valorNuevo) {
            setState(() {
              _valorPaisEmpleo = valorNuevo;
              paginaCargada = true;
            });
          },
          items: <String>['Colombia', 'Estados Unidos']
              .map<DropdownMenuItem<String>>((String valor) {
            return DropdownMenuItem<String>(
              value: valor,
              child: Text(valor, style: EstiloGeneral.h3NegroN),
            );
          }).toList(),
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: DropdownButton<String>(
          isExpanded: true,
          hint: const Text(
            'Tipo de Identificación',
            style: EstiloGeneral.h3GrisN,
          ),
          value: _valorTipoIdentificacion,
          icon: const Icon(
            Icons.keyboard_arrow_down,
            color: ColorGeneral.gris2,
          ),
          underline: const SizedBox(),
          onChanged: (String? valorNuevo) {
            setState(() {
              _valorTipoIdentificacion = valorNuevo;
              paginaCargada = true;
            });
          },
          items: <String>[
            'Cédula de Ciudadanía',
            'Cédula de Extranjería',
            'Pasaporte',
            'Permiso Especial',
          ].map<DropdownMenuItem<String>>((String valor) {
            return DropdownMenuItem<String>(
              value: valor,
              child: Text(valor, style: EstiloGeneral.h3NegroN),
            );
          }).toList(),
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: TextFormField(
          controller: _valorNumeroIdentificacion,
          decoration: const InputDecoration(
            labelText: 'Número de Identificación',
            border: InputBorder.none,
            labelStyle: TextStyle(color: ColorGeneral.gris2),
          ),
          onChanged: (String valor) {
            String respuesta = validarNumeroIdentificacion(valor);
            if (respuesta.isNotEmpty) {
              mostrarValidacion(context, '¡Atención!', respuesta);
            } else {
              setState(() {
                _valorNumeroIdentificacion.text = valor;
                _valorNumeroIdentificacion.selection =
                    TextSelection.fromPosition(
                  TextPosition(offset: _valorNumeroIdentificacion.text.length),
                );
                paginaCargada = true;
              });
            }
          },
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: Row(
          children: <Widget>[
            Flexible(
              child: TextField(
                controller: _valorFechaIngreso,
                enabled: false,
                decoration: InputDecoration(
                  hintText: 'Fecha de Ingreso: $valFechaIni',
                  hintStyle: EstiloGeneral.h3GrisN,
                  border: InputBorder.none,
                ),
              ),
            ),
            IconButton(
              icon: const Icon(
                Icons.date_range_outlined,
                color: ColorGeneral.gris2,
              ),
              padding: const EdgeInsets.all(0.0),
              onPressed: () {
                showDatePicker(
                  context: context,
                  initialDate: fecha,
                  firstDate: DateTime(fecha.year - 1),
                  lastDate: DateTime(fecha.year + 1),
                ).then((valor) {
                  setState(() {
                    _valorFechaIngreso.text = formatoFechaI(valor);
                    paginaCargada = true;
                  });
                });
              },
            ),
          ],
        ),
      ),
      EstiloGeneral.espaciadorX1,
      Container(
        width: obtenerAncho(pantalla),
        padding: EstiloGeneral.segundoPadding,
        decoration: EstiloGeneral.cajaBlanca,
        child: DropdownButton<String>(
          isExpanded: true,
          hint: const Text(
            'Área',
            style: EstiloGeneral.h3GrisN,
          ),
          value: _valorArea,
          icon: const Icon(
            Icons.keyboard_arrow_down,
            color: ColorGeneral.gris2,
          ),
          underline: const SizedBox(),
          onChanged: (String? valorNuevo) {
            setState(() {
              _valorArea = valorNuevo;
              paginaCargada = true;
            });
          },
          items: <String>[
            'Administración',
            'Financiera',
            'Compras',
            'Infraestructura',
            'Operación',
            'Talento Humano',
            'Servicios Varios',
          ].map<DropdownMenuItem<String>>((String valor) {
            return DropdownMenuItem<String>(
              value: valor,
              child: Text(valor, style: EstiloGeneral.h3NegroN),
            );
          }).toList(),
        ),
      ),
      EstiloGeneral.espaciadorX2,
      Directionality(
        textDirection: TextDirection.rtl,
        child: ElevatedButton.icon(
          icon: const Icon(Icons.keyboard_arrow_right, color: Colors.white),
          label: const Text('Guardar', style: EstiloGeneral.tBlancoBoton),
          style: EstiloGeneral.botonCyan,
          onPressed: () async {
            DateTime ahora = DateTime.now();
            ServicioEmpleados servicioEmpleados = ServicioEmpleados();
            String correoGenerado = generarCorreo(
              proEmpleados,
              _valorPrimerNombre.text,
              _valorPrimerApellido.text,
            );
            proEmpleado.id = proEmpleados.items[indice].id;
            proEmpleado.primerApellido = _valorPrimerApellido.text;
            proEmpleado.segundoApellido = _valorSegundoApellido.text;
            proEmpleado.primerNombre = _valorPrimerNombre.text;
            proEmpleado.otrosNombres = _valorOtrosNombres.text;
            proEmpleado.paisEmpleo = _valorPaisEmpleo;
            proEmpleado.tipoIdentificacion = _valorTipoIdentificacion;
            proEmpleado.numeroIdentificacion = _valorNumeroIdentificacion.text;
            proEmpleado.correoElectronico = correoGenerado;
            proEmpleado.fechaIngreso = formatoFechaBD(_valorFechaIngreso.text);
            proEmpleado.area = _valorArea;
            proEmpleado.estado = 'Activo';
            proEmpleado.fechaHoraRegistro =
                proEmpleados.items[indice].fechaHoraRegistro;
            proEmpleado.fechaHoraEdicion = formatoFechaG(ahora.toString());
            proEmpleado.actualizar();
            await servicioEmpleados.editar(context);
            Navigator.pushNamedAndRemoveUntil(
              context,
              'empleados',
              (Route<dynamic> route) => false,
            );
          },
        ),
      ),
      EstiloGeneral.espaciadorX3,
    ];

    return listado;
  }
}
