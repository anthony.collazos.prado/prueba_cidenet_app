import 'package:flutter/material.dart';
import 'package:prueba_cidenet/elementos/color_general.dart';

class EstiloGeneral {
  static final tema = ThemeData(
    fontFamily: 'Ubuntu',
    primaryColor: ColorGeneral.cyan,
    scaffoldBackgroundColor: ColorGeneral.gris1,
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8),
      ),
    ),
  );

  static const h1AzulB = TextStyle(
    fontSize: 26.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.azul,
  );
  static const h1CyanB = TextStyle(
    fontSize: 26.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.cyan,
  );
  static const h1NegroB = TextStyle(
    fontSize: 26.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.negro,
  );
  static const h2NegroB = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.negro,
  );
  static const h2CyanB = TextStyle(
    fontSize: 20.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.cyan,
  );
  static const h3NegroB = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.negro,
  );
  static const h3CyanB = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.cyan,
  );
  static const h3AzulB = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.azul,
  );
  static const h3RojoB = TextStyle(
    fontSize: 16.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.rojo,
  );
  static const h3NegroN = TextStyle(
    fontSize: 16.0,
    color: ColorGeneral.negro,
  );
  static const h3GrisN = TextStyle(
    fontSize: 16.0,
    color: ColorGeneral.gris2,
  );
  static const h4GrisN = TextStyle(
    fontSize: 13.0,
    color: ColorGeneral.gris2,
  );
  static const h4CyanB = TextStyle(
    fontSize: 13.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.cyan,
  );
  static const tGrisB = TextStyle(
    fontSize: 11.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.gris2,
    height: 1.5,
  );
  static const tGrisN = TextStyle(
    fontSize: 11.0,
    color: ColorGeneral.gris2,
    height: 1.5,
  );

  static const tBlancoBoton = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    color: Colors.white,
  );
  static const tAzulBoton = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.azul,
  );
  static const tGrisBoton = TextStyle(
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    color: ColorGeneral.gris2,
  );

  static const divisor1 = Divider(color: ColorGeneral.gris1);
  static const divisor2 = Divider(color: ColorGeneral.gris2);
  static const espaciadorX1 = SizedBox(height: 10.0);
  static const espaciadorX2 = SizedBox(height: 20.0);
  static const espaciadorX3 = SizedBox(height: 30.0);
  static const espaciadorY1 = SizedBox(width: 10.0);
  static const espaciadorY2 = SizedBox(width: 20.0);
  static const espaciadorY3 = SizedBox(width: 30.0);

  static const primerPadding = EdgeInsets.all(10.0);
  static const segundoPadding = EdgeInsets.symmetric(
    vertical: 10.0,
    horizontal: 15.0,
  );

  static final ButtonStyle botonBlanco = ElevatedButton.styleFrom(
    primary: Colors.white,
    padding: EdgeInsets.zero,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(30.0)),
    ),
  );
  static final ButtonStyle botonAzul = ElevatedButton.styleFrom(
    primary: ColorGeneral.azul,
    padding: const EdgeInsets.only(
      top: 10.0,
      right: 10.0,
      bottom: 10.0,
      left: 20.0,
    ),
    elevation: 0,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(30.0)),
    ),
  );
  static final ButtonStyle botonCyan = ElevatedButton.styleFrom(
    primary: ColorGeneral.cyan,
    padding: const EdgeInsets.only(
      top: 10.0,
      right: 10.0,
      bottom: 10.0,
      left: 20.0,
    ),
    elevation: 0,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(30.0)),
    ),
  );
  static final cajaBlanca = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(30.0),
    boxShadow: const <BoxShadow>[
      BoxShadow(
        color: ColorGeneral.gris2,
        offset: Offset(0.0, 2.0),
        blurRadius: 2.0,
      ),
    ],
  );

  static const circuloRojo = BoxDecoration(
    color: ColorGeneral.rojo,
    shape: BoxShape.circle,
  );
  static const circuloCyan = BoxDecoration(
    color: ColorGeneral.cyan,
    shape: BoxShape.circle,
  );
  static const circuloAzul = BoxDecoration(
    color: ColorGeneral.azul,
    shape: BoxShape.circle,
  );
}
