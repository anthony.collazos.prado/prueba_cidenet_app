import 'package:flutter/material.dart';
import 'package:prueba_cidenet/elementos/color_general.dart';
import 'package:prueba_cidenet/elementos/estilo_general.dart';
import 'package:prueba_cidenet/elementos/imagen_general.dart';
import 'package:prueba_cidenet/servicios/servicio_empleados.dart';

PreferredSizeWidget appBarGeneral(BuildContext context) {
  return AppBar(
    leading: Navigator.canPop(context)
        ? Padding(
            padding: const EdgeInsets.only(top: 18.0, left: 10.0),
            child: IconButton(
              icon: const Icon(
                Icons.keyboard_arrow_left,
                color: ColorGeneral.negro,
                size: 33.0,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          )
        : null,
    title: SafeArea(
      child: SizedBox(
        width: 40.0,
        height: 60.0,
        child: ImagenGeneral.isotipoNegro,
      ),
    ),
    centerTitle: true,
    backgroundColor: Colors.transparent,
    elevation: 0,
    actions: <Widget>[
      Padding(
        padding: const EdgeInsets.only(top: 15.0, right: 10.0),
        child: IconButton(
          icon: const Icon(Icons.menu, color: ColorGeneral.negro, size: 30.0),
          onPressed: () {},
        ),
      ),
    ],
  );
}

Widget pantallaCargando() {
  return Scaffold(
    extendBodyBehindAppBar: true,
    body: Container(
      constraints: const BoxConstraints.expand(),
      color: ColorGeneral.gris1,
      child: SafeArea(
        child: Center(
          child: SizedBox(
            width: 100.0,
            height: 120.0,
            child: Column(
              children: [
                SizedBox(
                    width: 80.0, height: 80.0, child: ImagenGeneral.logoNegro),
                EstiloGeneral.espaciadorX2,
                const Text('Cargando...', style: EstiloGeneral.h3GrisN),
              ],
            ),
          ),
        ),
      ),
    ),
  );
}

void mostrarMensaje(
    BuildContext context, String titulo, String mensaje, int id) {
  ServicioEmpleados servicioEmpleados = ServicioEmpleados();

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        contentPadding: EstiloGeneral.primerPadding,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: <Widget>[
                const Icon(
                  Icons.message_outlined,
                  color: ColorGeneral.cyan,
                ),
                EstiloGeneral.espaciadorY1,
                Text(titulo, style: EstiloGeneral.h2CyanB),
              ],
            ),
            EstiloGeneral.espaciadorX1,
            EstiloGeneral.divisor1,
            EstiloGeneral.espaciadorX1,
            Text(
              mensaje,
              style: EstiloGeneral.h3NegroN,
              textAlign: TextAlign.center,
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () async {
              await servicioEmpleados.eliminar(id);
              Navigator.pushNamedAndRemoveUntil(
                context,
                'empleados',
                (Route<dynamic> route) => false,
              );
            },
            child: const Text('SI', style: EstiloGeneral.h3AzulB),
          ),
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('NO', style: EstiloGeneral.h3CyanB),
          ),
        ],
      );
    },
  );
}

void mostrarValidacion(BuildContext context, String titulo, String mensaje) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        contentPadding: EstiloGeneral.primerPadding,
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: <Widget>[
                const Icon(
                  Icons.message_outlined,
                  color: ColorGeneral.cyan,
                ),
                EstiloGeneral.espaciadorY1,
                Text(titulo, style: EstiloGeneral.h2CyanB),
              ],
            ),
            EstiloGeneral.espaciadorX1,
            EstiloGeneral.divisor1,
            EstiloGeneral.espaciadorX1,
            Text(
              mensaje,
              style: EstiloGeneral.h3NegroN,
              textAlign: TextAlign.center,
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () => Navigator.pop(context),
            child: const Text('LISTO', style: EstiloGeneral.h3CyanB),
          ),
        ],
      );
    },
  );
}

double obtenerMarco(Size pantalla) => pantalla.width * 0.04;
double obtenerAncho(Size pantalla) => pantalla.width * 0.96;
