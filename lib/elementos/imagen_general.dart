import 'package:flutter/material.dart';

class ImagenGeneral {
  static String base = 'assets/images/';
  static final logoNegro = Image.asset('${base}logo_cidenet_negro.png');
  static final logoBlanco = Image.asset('${base}logo_cidenet_blanco.png');
  static final isotipoNegro = Image.asset('${base}isotipo_cidenet_negro.png');
}
