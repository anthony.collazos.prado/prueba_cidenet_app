import 'package:flutter/cupertino.dart';

class ColorGeneral {
  static const Color negro = Color(0xFF1B1919);
  static const Color gris1 = Color(0xFFF5F5F8);
  static const Color gris2 = Color(0xFFADADAD);
  static const Color cyan = Color(0xFF24B5D4);
  static const Color azul = Color(0xFF004EB9);
  static const Color rojo = Color(0xFFDC6417);
}
