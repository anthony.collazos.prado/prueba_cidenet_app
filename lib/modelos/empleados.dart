import 'package:flutter/cupertino.dart';
import 'package:prueba_cidenet/modelos/empleado.dart';

class Empleados extends ChangeNotifier {
  final List<Empleado> items = [];

  void agregar(Empleado item) {
    items.add(item);
    notifyListeners();
  }

  void eliminarTodo() {
    items.clear();
    notifyListeners();
  }
}
