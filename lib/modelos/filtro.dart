import 'package:flutter/material.dart';

class Filtro extends ChangeNotifier {
  String? opcion;
  String? palabra;

  Filtro({
    this.opcion,
    this.palabra,
  });

  void actualizar() {
    notifyListeners();
  }

  void limpiarTodo() {
    opcion = '';
    palabra = '';
    notifyListeners();
  }
}
