import 'package:flutter/material.dart';

class Empleado extends ChangeNotifier {
  int? id;
  String? primerApellido;
  String? segundoApellido;
  String? primerNombre;
  String? otrosNombres;
  String? paisEmpleo;
  String? tipoIdentificacion;
  String? numeroIdentificacion;
  String? correoElectronico;
  String? fechaIngreso;
  String? area;
  String? estado;
  String? fechaHoraRegistro;
  String? fechaHoraEdicion;

  Empleado({
    this.id,
    this.primerApellido,
    this.segundoApellido,
    this.primerNombre,
    this.otrosNombres,
    this.paisEmpleo,
    this.tipoIdentificacion,
    this.numeroIdentificacion,
    this.correoElectronico,
    this.fechaIngreso,
    this.area,
    this.estado,
    this.fechaHoraRegistro,
    this.fechaHoraEdicion,
  });

  void actualizar() {
    notifyListeners();
  }

  void limpiarTodo() {
    id = 0;
    primerApellido = '';
    segundoApellido = '';
    primerNombre = '';
    otrosNombres = '';
    paisEmpleo = '';
    tipoIdentificacion = '';
    numeroIdentificacion = '';
    correoElectronico = '';
    fechaIngreso = '';
    area = '';
    estado = '';
    fechaHoraRegistro = '';
    fechaHoraEdicion = '';
    notifyListeners();
  }
}
